package com.nico.tdif26app;

import android.app.AlertDialog;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;

public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        ((TextView) this.findViewById(R.id.message)).setText(R.string.calling_server);

        Bundle intentParams = getIntent().getExtras();
        String inputLogin = intentParams.getString("inputLogin");
        String inputPasswd = intentParams.getString("inputPasswd");

        HttpRequestMaker sendRequest = new HttpRequestMaker(this);
        sendRequest.execute("http://totoboum.byethost15.com/auth.php?login=" + inputLogin + "&passwd=" + inputPasswd);
    }

    public void displayGetResult(String messageToDisplay) {
        ((TextView) this.findViewById(R.id.message)).setText(messageToDisplay);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    public void changeScreen(View view) {
        // TODO: Continue here.
    }


}

package com.nico.tdif26app;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Nico on 20/10/13.
 */
public class HttpRequestMaker extends AsyncTask<String, Integer, String> {

    static final int STATUS_WRONG_PASSWORD = 0;
    static final int STATUS_OK = 1;
    static final int STATUS_NO_SUCH_USER = 2;
    static final int STATUS_ACCOUNT_BLOCKED = 3;

    private SecondActivity context;

    public HttpRequestMaker(SecondActivity activity) {
        this.context = activity;
    }
    public String getResponseMessageToDisplay(JSONObject serverResponse) {
        String message;
        if (serverResponse != null) {
            int status = 999;
            try {
                status = serverResponse.getInt("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            switch (status) {
                case STATUS_WRONG_PASSWORD:
                    int nbrTriesLeft = 0;
                    try {
                        nbrTriesLeft = serverResponse.getInt("triesLeft");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    message = "Wrong password ! Only " + nbrTriesLeft + "tries left.";
                    break;
                case STATUS_OK:
                    message = "Ok, you are connected !";
                    break;
                case STATUS_NO_SUCH_USER:
                    message = "No such user.";
                    break;
                case STATUS_ACCOUNT_BLOCKED:
                    message = "Account blocked. Please contact an admin.";
                    break;
                default:
                    message = "Error. Server answer is not correct.";
                    break;
            }
        } else {
            message = "Server response is null.";
        }
        return message;
    }

    public JSONObject getServerResponse(String url) {
        HttpGet httpGet = new HttpGet(url);
        //httpGet.getParams().setParameter("login", login);
        //httpGet.getParams().setParameter("passwd", passwd);

        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = null;

        // Execute GET query
        try {
            httpResponse = httpClient.execute(httpGet);
        }
        catch (ClientProtocolException clientProtocolException) {
            clientProtocolException.printStackTrace();
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
        }

        if (httpResponse != null) {
            // Read content of the response (get the JSON).
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            String json = null;
            try {
                json = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Create a JSONArray with the string retrieved from the server response.
            JSONTokener tokener = new JSONTokener(json);
            JSONObject serverResponse = null;
            try {
                serverResponse = new JSONObject(tokener);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return serverResponse;
        } else {
            return null;
        }
    }

    @Override
    protected String doInBackground(String... url) {
        String messageToDisplay = new String();
        JSONObject serverResponse = getServerResponse(url[0]);
        if (serverResponse != null) {
            messageToDisplay = getResponseMessageToDisplay(serverResponse);
        } else {
            messageToDisplay = "Error calling server.";
        }
        return messageToDisplay;
    }

    @Override
    protected void onPostExecute(String messageToDisplay) {
        ((TextView) context.findViewById(R.id.message)).setText(messageToDisplay);
    }

}

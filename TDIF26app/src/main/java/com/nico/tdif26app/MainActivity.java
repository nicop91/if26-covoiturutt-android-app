package com.nico.tdif26app;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.nico.tdif26app.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
		// Another comment
		//commentaire
        return true;
    }

    public void connexion(View view) {
        EditText login = (EditText) this.findViewById(R.id.login);
        EditText passwd = (EditText) this.findViewById(R.id.password);

        Intent intent = new Intent(this, SecondActivity.class);
        Bundle intentParams = new Bundle();
        intentParams.putString("inputLogin", login.getText().toString());
        intentParams.putString("inputPasswd", passwd.getText().toString());
        intent.putExtras(intentParams);

        startActivity(intent);
    }
}
